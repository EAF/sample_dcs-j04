package main;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import observer.Observateur;

import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.graphics.Color;

import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.DisposeEvent;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import socket.CupptSocket;
import thread.ObservableListener;

public class Form1 {

	protected Shell shlCupptSdk;
	protected Display display;

	private Text textCUPPSPLT, textCUPPSPN, textCUPPSOPR, textCUPPSCN, textCUPPSPP, textXSDU,
			textCUPPSUN, textCUPPSACN;

	private StyledText styledTextGoals, styledTextXML, styledTextEvent, styledTextXMLValidate;

	private Button btnInterfacelevelsavailablerequest, btnConnection, btnInterfacelevelrequest,
			btnObsoleteInterfacelevelrequest, btnUnsupportedInterfacelevelrequest;

	private TabFolder tabFolder;
	private TabItem tabPageLesson1, tabPageLesson2, tabPageLesson3;

	private Table tableEvents, tableInterfaceLevels, tableDeviceQueryList;

	private Label lblDeviceList;

	private Text textDeviceToken;
	private Label lblDeviceToken;

	private TableColumn tableColumn;
	private TableColumn tableColumn_1;
	private TableColumn tableColumn_2;
	private TableColumn tableColumn_3;
	private TableColumn tableColumn_4;
	private TableColumn tableColumn_5;
	private TableColumn tableColumn_6;
	private TableColumn tableColumn_7;
	private TableColumn tableColumn_8;
	private TableColumn tableColumn_9;
	private TableColumn tableColumn_10;
	private TableColumn tableColumn_11;
	private Button btnDeviceQueryRequest;
	private Text textHostName;
	private Text textHostIP;
	private Text textHostPort;
	private TabItem tabPageLesson20;
	private Button btnByeRequest;
	private StyledText styledTextByeResponse;
	private Label labelByeResponse;
	private Button btnDisconnect;
	private TabItem tabPageLesson4;
	private Composite compositeLesson4;
	private Table tableDeviceAcquireList;
	private TableColumn tableColumn_12;
	private TableColumn tableColumn_14;
	private TableColumn tableColumn_15;
	private Label label_1;
	private Button btnAuthenticateRequest;
	private Button btnDeviceAcquireRequest;
	private Table tableConnectedDeviceList;
	private TableColumn tableColumn_19;
	private TableColumn tableColumn_21;
	private TableColumn tableColumn_22;
	private TableColumn tableColumn_25;
	private Label labelConnectedDeviceList;
	private Label lblNewLabel_2;
	private Label lblGreenReady;
	private Label lblOrangePaperjam;
	private Label lblRed;

	//
	private CupptSocket platformSocket = new CupptSocket();

	private ArrayList<CupptSocket> deviceSocketList = new ArrayList<CupptSocket>();

	// Vars
	String AirlineCode = "FR";

	String ApplicationCode = "EAS2012CUPPT0007";
	String ApplicationName = "Sample_DCS-J04";
	String ApplicationVersion = "01.03";
	String ApplicationData;

	
	String DeviceToken;
	String DeviceIP;
	int DevicePort;

	// XSD informations
	String InterfaceLevel;
	String XSDLocalPath;

	String XSDVersion;

	// colors

	Color green = SWTResourceManager.getColor(50, 205, 50);
	Color orange = SWTResourceManager.getColor(255, 165, 0);
	Color red = SWTResourceManager.getColor(SWT.COLOR_RED);

	//

	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Form1 window = new Form1();
			window.open();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		display = Display.getDefault();
		createContents();
		shlCupptSdk.open();
		shlCupptSdk.layout();
		while (!shlCupptSdk.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();

			}
		}

	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shlCupptSdk = new Shell();
		shlCupptSdk.addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {

				System.out.println("closing...");
				disconnect();

			}
		});

		shlCupptSdk.setImage(SWTResourceManager.getImage(Form1.class, "/main/icon53.png"));
		shlCupptSdk.setMinimumSize(new Point(971, 649));
		shlCupptSdk.setSize(1024, 649);
		shlCupptSdk.setText("CUPPT SDK - lesson 04 Java");

		Group grpPlatformConnection = new Group(shlCupptSdk, SWT.NONE);
		grpPlatformConnection.setText("Platform Connection");
		grpPlatformConnection.setBounds(10, 10, 988, 105);

		Label lblSitecuppsplt = new Label(grpPlatformConnection, SWT.NONE);
		lblSitecuppsplt.setBounds(10, 22, 101, 15);
		lblSitecuppsplt.setText("Site (CUPPSPLT)");

		textCUPPSPLT = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPLT.setBounds(117, 19, 89, 21);

		Label lblNodecuppspn = new Label(grpPlatformConnection, SWT.NONE);
		lblNodecuppspn.setText("Node (CUPPSPN)");
		lblNodecuppspn.setBounds(212, 22, 101, 15);

		textCUPPSPN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPN.setBounds(319, 19, 89, 21);

		Label lblOpratorcuppsopr = new Label(grpPlatformConnection, SWT.NONE);
		lblOpratorcuppsopr.setText("Operator (CUPPSOPR)");
		lblOpratorcuppsopr.setBounds(454, 22, 123, 15);

		textCUPPSOPR = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSOPR.setBounds(583, 19, 89, 21);

		Label lblComputerNamecuppscn = new Label(grpPlatformConnection, SWT.NONE);
		lblComputerNamecuppscn.setText("Computer Name (CUPPSCN)");
		lblComputerNamecuppscn.setBounds(704, 22, 160, 15);

		textCUPPSCN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSCN.setBounds(870, 19, 108, 21);

		Label lblPortcuppspp = new Label(grpPlatformConnection, SWT.NONE);
		lblPortcuppspp.setText("Port (CUPPSPP)");
		lblPortcuppspp.setBounds(212, 49, 101, 15);

		textCUPPSPP = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSPP.setBounds(319, 46, 89, 21);

		Label lblXsdSchema = new Label(grpPlatformConnection, SWT.NONE);
		lblXsdSchema.setText("XSD");
		lblXsdSchema.setBounds(212, 77, 101, 15);

		textXSDU = new Text(grpPlatformConnection, SWT.BORDER);
		textXSDU.setBounds(319, 74, 353, 21);

		Label lblUsernamecuppsun = new Label(grpPlatformConnection, SWT.NONE);
		lblUsernamecuppsun.setText("UserName (CUPPSUN)");
		lblUsernamecuppsun.setBounds(454, 49, 123, 15);

		textCUPPSUN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSUN.setBounds(583, 46, 89, 21);

		Label lblAlternateNamecuppsacn = new Label(grpPlatformConnection, SWT.NONE);
		lblAlternateNamecuppsacn.setText("Alternate Name (CUPPSACN)");
		lblAlternateNamecuppsacn.setBounds(704, 49, 160, 15);

		textCUPPSACN = new Text(grpPlatformConnection, SWT.BORDER);
		textCUPPSACN.setBounds(870, 46, 108, 21);

		tabFolder = new TabFolder(shlCupptSdk, SWT.NONE);
		tabFolder.setBounds(10, 121, 711, 336);

		tabPageLesson1 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson1.setText("Lesson 1");

		Composite compositeLesson1 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson1.setControl(compositeLesson1);

		btnConnection = new Button(compositeLesson1, SWT.NONE);
		btnConnection.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonConnect_Click(e);
			}
		});
		btnConnection.setAlignment(SWT.LEFT);

		btnConnection.setBounds(496, 36, 197, 25);
		btnConnection.setText("1 - Network Socket Connection");

		btnInterfacelevelsavailablerequest = new Button(compositeLesson1, SWT.NONE);
		btnInterfacelevelsavailablerequest.setEnabled(false);
		btnInterfacelevelsavailablerequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnInterfacelevelsavailablerequest_Click(e);
			}
		});
		btnInterfacelevelsavailablerequest.setAlignment(SWT.LEFT);
		btnInterfacelevelsavailablerequest.setText("2 - InterfaceLevelsAvailableRequest");
		btnInterfacelevelsavailablerequest.setBounds(496, 67, 197, 25);

		styledTextXML = new StyledText(compositeLesson1, SWT.BORDER);
		styledTextXML.setBounds(10, 36, 480, 100);
		styledTextXML.setWordWrap(true);

		styledTextEvent = new StyledText(compositeLesson1, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		styledTextEvent.setBounds(10, 142, 683, 156);
		styledTextEvent.setWordWrap(true);

		Label label = new Label(compositeLesson1, SWT.NONE);
		label.setText("XML Message");
		label.setForeground(display.getSystemColor(SWT.COLOR_LIST_SELECTION));
		label.setAlignment(SWT.CENTER);
		label.setBounds(10, 15, 427, 15);

		tabPageLesson2 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson2.setText("Lesson 2");

		Composite compositeLesson2 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson2.setControl(compositeLesson2);

		Label lblNewLabel = new Label(compositeLesson2, SWT.NONE);
		lblNewLabel.setForeground(display.getSystemColor(SWT.COLOR_LIST_SELECTION));
		lblNewLabel.setBounds(10, 10, 187, 15);
		lblNewLabel.setText("Interface Levels Available List");

		tableInterfaceLevels = new Table(compositeLesson2, SWT.BORDER | SWT.FULL_SELECTION);
		tableInterfaceLevels.setBounds(10, 27, 480, 120);
		tableInterfaceLevels.setHeaderVisible(true);
		tableInterfaceLevels.setLinesVisible(true);

		tableColumn_9 = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tableColumn_9.setWidth(73);
		tableColumn_9.setText("Level");

		tableColumn_10 = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tableColumn_10.setWidth(284);
		tableColumn_10.setText("Local Path");

		tableColumn_11 = new TableColumn(tableInterfaceLevels, SWT.NONE);
		tableColumn_11.setWidth(119);
		tableColumn_11.setText("XSD Version");

		btnInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnInterfacelevelrequest.setEnabled(false);
		btnInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonInterfacesLevelRequest_Click(e);
			}
		});
		btnInterfacelevelrequest.setText("3 - InterfaceLevelRequest");
		btnInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnInterfacelevelrequest.setBounds(496, 27, 197, 25);

		Label lblXmlMessageResponse = new Label(compositeLesson2, SWT.NONE);
		lblXmlMessageResponse.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblXmlMessageResponse.setText("XML Message Response Validation");
		lblXmlMessageResponse.setBounds(10, 153, 187, 15);

		styledTextXMLValidate = new StyledText(compositeLesson2, SWT.BORDER | SWT.MULTI
				| SWT.V_SCROLL);
		styledTextXMLValidate.setBounds(10, 170, 683, 130);
		styledTextXMLValidate.setWordWrap(true);

		btnObsoleteInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnObsoleteInterfacelevelrequest.setEnabled(false);
		btnObsoleteInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				buttonObsoleteInterfacesLevelRequest_Click(e);
			}
		});
		btnObsoleteInterfacelevelrequest.setForeground(SWTResourceManager.getColor(255, 0, 0));
		btnObsoleteInterfacelevelrequest.setBackground(SWTResourceManager.getColor(255, 0, 0));
		btnObsoleteInterfacelevelrequest.setText("Obsolete InterfaceLevelRequest");
		btnObsoleteInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnObsoleteInterfacelevelrequest.setBounds(496, 122, 197, 25);

		btnUnsupportedInterfacelevelrequest = new Button(compositeLesson2, SWT.NONE);
		btnUnsupportedInterfacelevelrequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnUnsuportedInterfacelevelrequest_Click(e);
			}
		});
		btnUnsupportedInterfacelevelrequest.setText("Unsupported InterfaceLevelRequest");
		btnUnsupportedInterfacelevelrequest.setForeground(SWTResourceManager.getColor(255, 0, 0));
		btnUnsupportedInterfacelevelrequest.setEnabled(false);
		btnUnsupportedInterfacelevelrequest.setBackground(SWTResourceManager.getColor(255, 0, 0));
		btnUnsupportedInterfacelevelrequest.setAlignment(SWT.LEFT);
		btnUnsupportedInterfacelevelrequest.setBounds(496, 91, 197, 25);

		Label lblNewLabel_1 = new Label(compositeLesson2, SWT.NONE);
		lblNewLabel_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblNewLabel_1.setBounds(496, 70, 197, 15);
		lblNewLabel_1.setText("InterfaceLevelErrorEvent");

		tabPageLesson3 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson3.setText("Lesson 3");

		Composite compositeLesson3 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson3.setControl(compositeLesson3);

		tableDeviceQueryList = new Table(compositeLesson3, SWT.BORDER | SWT.FULL_SELECTION);
		tableDeviceQueryList.setLinesVisible(true);
		tableDeviceQueryList.setHeaderVisible(true);
		tableDeviceQueryList.setBounds(10, 36, 535, 262);

		tableColumn = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn.setWidth(120);
		tableColumn.setText("DeviceName");

		tableColumn_1 = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn_1.setWidth(110);
		tableColumn_1.setText("HostName");

		tableColumn_2 = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn_2.setWidth(62);
		tableColumn_2.setText("IP");

		tableColumn_3 = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn_3.setWidth(48);
		tableColumn_3.setText("Port");

		tableColumn_4 = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn_4.setWidth(69);
		tableColumn_4.setText("Vendor");

		tableColumn_5 = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn_5.setWidth(55);
		tableColumn_5.setText("Model");

		tableColumn_6 = new TableColumn(tableDeviceQueryList, SWT.NONE);
		tableColumn_6.setWidth(67);
		tableColumn_6.setText("Status");

		lblDeviceList = new Label(compositeLesson3, SWT.NONE);
		lblDeviceList.setText("Device List");
		lblDeviceList.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblDeviceList.setBounds(10, 15, 187, 15);

		textDeviceToken = new Text(compositeLesson3, SWT.BORDER);
		textDeviceToken.setEditable(false);
		textDeviceToken.setBounds(551, 12, 142, 21);

		lblDeviceToken = new Label(compositeLesson3, SWT.NONE);
		lblDeviceToken.setFont(SWTResourceManager.getFont("Segoe UI", 14, SWT.BOLD));
		lblDeviceToken.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		lblDeviceToken.setText("Device Token");
		lblDeviceToken.setBounds(421, 10, 124, 21);

		btnDeviceQueryRequest = new Button(compositeLesson3, SWT.NONE);
		btnDeviceQueryRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDeviceQueryRequest_Click(e);
			}
		});
		btnDeviceQueryRequest.setText("5 - Device Query Request");
		btnDeviceQueryRequest.setEnabled(false);
		btnDeviceQueryRequest.setAlignment(SWT.LEFT);
		btnDeviceQueryRequest.setBounds(551, 70, 142, 25);

		btnAuthenticateRequest = new Button(compositeLesson3, SWT.NONE);
		btnAuthenticateRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnAuthenticateRequest_Click(e);
			}
		});
		btnAuthenticateRequest.setText("4 - Authenticate Request");
		btnAuthenticateRequest.setEnabled(false);
		btnAuthenticateRequest.setAlignment(SWT.LEFT);
		btnAuthenticateRequest.setBounds(551, 36, 142, 25);

		tabPageLesson4 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson4.setText("Lesson 4");

		compositeLesson4 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson4.setControl(compositeLesson4);

		tableDeviceAcquireList = new Table(compositeLesson4, SWT.BORDER | SWT.FULL_SELECTION);
		tableDeviceAcquireList.setLinesVisible(true);
		tableDeviceAcquireList.setHeaderVisible(true);
		tableDeviceAcquireList.setBounds(0, 31, 265, 262);

		tableColumn_12 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_12.setWidth(120);
		tableColumn_12.setText("DeviceName");

		tableColumn_14 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_14.setWidth(90);
		tableColumn_14.setText("IP");

		tableColumn_15 = new TableColumn(tableDeviceAcquireList, SWT.NONE);
		tableColumn_15.setWidth(50);
		tableColumn_15.setText("Port");

		label_1 = new Label(compositeLesson4, SWT.NONE);
		label_1.setText("Device List");
		label_1.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		label_1.setBounds(10, 10, 187, 15);

		Group grpDeviceAcquireRequest = new Group(compositeLesson4, SWT.NONE);
		grpDeviceAcquireRequest.setLocation(278, 62);
		grpDeviceAcquireRequest.setSize(142, 166);
		grpDeviceAcquireRequest.setText("Relay to");

		Label lblHostname = new Label(grpDeviceAcquireRequest, SWT.NONE);
		lblHostname.setBounds(10, 21, 55, 15);
		lblHostname.setText("hostName");

		Label lblIp = new Label(grpDeviceAcquireRequest, SWT.NONE);
		lblIp.setText("IP");
		lblIp.setBounds(10, 69, 55, 15);

		Label lblPort = new Label(grpDeviceAcquireRequest, SWT.NONE);
		lblPort.setText("Port");
		lblPort.setBounds(10, 117, 55, 15);

		textHostName = new Text(grpDeviceAcquireRequest, SWT.BORDER);
		textHostName.setBounds(10, 42, 122, 21);

		textHostIP = new Text(grpDeviceAcquireRequest, SWT.BORDER);
		textHostIP.setBounds(10, 90, 122, 21);

		textHostPort = new Text(grpDeviceAcquireRequest, SWT.BORDER);
		textHostPort.setBounds(10, 138, 76, 21);

		btnDeviceAcquireRequest = new Button(compositeLesson4, SWT.NONE);
		btnDeviceAcquireRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDeviceAcquireRequest_Click(e);
			}
		});
		btnDeviceAcquireRequest.setText("6 - Device Acq. Request");
		btnDeviceAcquireRequest.setEnabled(false);
		btnDeviceAcquireRequest.setAlignment(SWT.LEFT);
		btnDeviceAcquireRequest.setBounds(278, 31, 142, 25);

		tableConnectedDeviceList = new Table(compositeLesson4, SWT.BORDER | SWT.FULL_SELECTION);
		tableConnectedDeviceList.setLinesVisible(true);
		tableConnectedDeviceList.setHeaderVisible(true);
		tableConnectedDeviceList.setBounds(426, 31, 265, 262);

		tableColumn_19 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_19.setWidth(120);
		tableColumn_19.setText("DeviceName");

		tableColumn_21 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_21.setWidth(62);
		tableColumn_21.setText("IP");

		tableColumn_22 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_22.setWidth(48);
		tableColumn_22.setText("Port");

		tableColumn_25 = new TableColumn(tableConnectedDeviceList, SWT.NONE);
		tableColumn_25.setWidth(67);
		tableColumn_25.setText("Status");

		labelConnectedDeviceList = new Label(compositeLesson4, SWT.NONE);
		labelConnectedDeviceList.setText("Connected device List");
		labelConnectedDeviceList.setForeground(SWTResourceManager
				.getColor(SWT.COLOR_LIST_SELECTION));
		labelConnectedDeviceList.setBounds(426, 10, 187, 15);

		lblNewLabel_2 = new Label(compositeLesson4, SWT.NONE);
		lblNewLabel_2.setForeground(SWTResourceManager.getColor(255, 165, 0));
		lblNewLabel_2.setBounds(281, 250, 139, 15);
		lblNewLabel_2.setText("Paperout - Paperjam");

		lblGreenReady = new Label(compositeLesson4, SWT.NONE);
		lblGreenReady.setForeground(SWTResourceManager.getColor(50, 205, 50));
		lblGreenReady.setText("Ready");
		lblGreenReady.setBounds(281, 235, 139, 15);

		lblOrangePaperjam = new Label(compositeLesson4, SWT.NONE);
		lblOrangePaperjam.setForeground(SWTResourceManager.getColor(255, 165, 0));
		lblOrangePaperjam.setText("diskError - init");
		lblOrangePaperjam.setBounds(281, 265, 139, 15);

		lblRed = new Label(compositeLesson4, SWT.NONE);
		lblRed.setForeground(SWTResourceManager.getColor(SWT.COLOR_RED));
		lblRed.setText("Offline - PowerOff");
		lblRed.setBounds(281, 280, 139, 15);

		tabPageLesson20 = new TabItem(tabFolder, SWT.NONE);
		tabPageLesson20.setText("Lesson 20");

		Composite compositeLesson20 = new Composite(tabFolder, SWT.NONE);
		tabPageLesson20.setControl(compositeLesson20);

		btnByeRequest = new Button(compositeLesson20, SWT.NONE);
		btnByeRequest.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnByeRequest_Click(e);
			}
		});
		btnByeRequest.setText("Bye Request");
		btnByeRequest.setBounds(496, 31, 197, 25);

		styledTextByeResponse = new StyledText(compositeLesson20, SWT.BORDER);
		styledTextByeResponse.setWordWrap(true);
		styledTextByeResponse.setBounds(10, 31, 480, 100);

		labelByeResponse = new Label(compositeLesson20, SWT.NONE);
		labelByeResponse.setText("byeResponse");
		labelByeResponse.setForeground(SWTResourceManager.getColor(SWT.COLOR_LIST_SELECTION));
		labelByeResponse.setBounds(10, 10, 100, 15);

		btnDisconnect = new Button(compositeLesson20, SWT.NONE);
		btnDisconnect.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				btnDisconnect_Click(e);
			}
		});
		btnDisconnect.setText("Disconnect");
		btnDisconnect.setBounds(496, 62, 197, 25);

		tableEvents = new Table(shlCupptSdk, SWT.BORDER | SWT.FULL_SELECTION);
		tableEvents.setBounds(10, 463, 711, 138);
		tableEvents.setHeaderVisible(true);
		tableEvents.setLinesVisible(true);

		tableColumn_7 = new TableColumn(tableEvents, SWT.NONE);
		tableColumn_7.setWidth(156);
		tableColumn_7.setText("Time");

		tableColumn_8 = new TableColumn(tableEvents, SWT.NONE);
		tableColumn_8.setWidth(550);
		tableColumn_8.setText("Event");

		Group grpLessonGoals = new Group(shlCupptSdk, SWT.NONE);
		grpLessonGoals.setText("Lesson Goals");
		grpLessonGoals.setBounds(727, 137, 271, 464);

		styledTextGoals = new StyledText(grpLessonGoals, SWT.BORDER);
		styledTextGoals.setBackground(SWTResourceManager.getColor(255, 255, 204));
		styledTextGoals.setBounds(10, 23, 251, 431);
		styledTextGoals.setWordWrap(true);

		loadEnvironmentVariables();
		loadGoals();

	}

	// load Environment Variables
	private void loadEnvironmentVariables() {
		try {
			textCUPPSPN.setText(System.getenv("CUPPSPN"));
			textCUPPSPP.setText(System.getenv("CUPPSPP"));

			textCUPPSPLT.setText(System.getenv("CUPPSPLT"));
			textCUPPSCN.setText(System.getenv("CUPPSCN"));
			textCUPPSACN.setText(System.getenv("CUPPSACN"));

			textCUPPSOPR.setText(System.getenv("CUPPSOPR"));
			textCUPPSUN.setText(System.getenv("CUPPSUN"));

			textXSDU.setText(System.getenv("CUPPSXSDU"));
		} catch (Exception ec) {
			tableEventAddLine("Error getting Environment variables:" + ec);
		}

	}

	// load text goals
	private void loadGoals() {
		styledTextGoals.append("1. Send the <deviceQueryRequest> message \n");
		styledTextGoals.append("\n");
		styledTextGoals.append("2. Read and parse the <deviceQueryResponse> \n");
		styledTextGoals.append("\n");
		styledTextGoals.append("3. Get the list of devices handled by the platform \n");
	}

	private void buttonConnect_Click(SelectionEvent e) {

		try {
			if (!platformSocket.getSocket().isConnected() || platformSocket.getSocket().isClosed()) {

				String IP = textCUPPSPN.getText();
				int port = Integer.parseInt(textCUPPSPP.getText());

				platformSocket = new CupptSocket(IP, port, "platform");

				tableEventAddLine("Connected to " + IP + ":" + port);

				// Init ObservableListener
				final ObservableListener observablePlatformListener = new ObservableListener(
						platformSocket);

				platformSocket.setObservableListener(observablePlatformListener);

				// Add observator

				platformSocket.getObservableListener().addObservateur(new Observateur() {
					public void update(String message) {

						final String XML = message;

						Display.getDefault().asyncExec(new Runnable() {
							public void run() {

								CupptSocket socket = observablePlatformListener.getSocket();

								styledTextEventAppend('R', XML);

								tableEventAddLine("Message " + socket.getIP() + ":"
										+ socket.getPort() + " <-- " + XML);

								XMLReader(XML, socket);
							}
						});

					}
				});

				btnInterfacelevelsavailablerequest.setEnabled(true);

			} else {
				System.out.println("Already connected");

				dialog("Already connected", SWT.ICON_WARNING | SWT.OK);

			}
		} catch (Exception ec) {
			tableEventAddLine("Error connecting to server : " + ec);
			dialog("Error connecting to server : " + ec, SWT.ICON_ERROR);
			disconnect();
		}

	}

	private void btnInterfacelevelsavailablerequest_Click(SelectionEvent e) {
		try {

			tableInterfaceLevels.removeAll();

			sendXML("interfaceLevelsAvailableRequest");

		} catch (Exception E) {
			System.out.println(E.toString());
		}
	}

	private void buttonInterfacesLevelRequest_Click(SelectionEvent e) {

		if (textDeviceToken.getText().equals("")) {

			tableDeviceQueryList.removeAll();

			if (tableInterfaceLevels.getSelectionCount() > 0) {

				//
				for (TableItem selectedItem : tableInterfaceLevels.getSelection()) {

					InterfaceLevel = selectedItem.getText(0);
					XSDLocalPath = selectedItem.getText(1);
					XSDVersion = selectedItem.getText(2);

				}

				generateXML("interfaceLevelRequest");

				// send message
				SendMsg(styledTextXML.getText(), platformSocket);

				btnAuthenticateRequest.setEnabled(true);
				tabFolder.setSelection(2);

			} else {

				dialog("Please select Interface Level", SWT.ICON_INFORMATION | SWT.OK);

			}

		} else {
			dialog("Already authenticated", SWT.ICON_WARNING | SWT.OK);
		}

	}

	private void buttonObsoleteInterfacesLevelRequest_Click(SelectionEvent e) {

		InterfaceLevel = "01.00";
		XSDLocalPath = "01.00";
		XSDVersion = "01.00";

		sendXML("interfaceLevelRequest");

	}

	private void btnUnsuportedInterfacelevelrequest_Click(SelectionEvent e) {
		InterfaceLevel = "01.02";
		XSDLocalPath = "01.02";
		XSDVersion = "01.02";

		sendXML("interfaceLevelRequest");

	}

	private void btnAuthenticateRequest_Click(SelectionEvent e) {

		// if (textDeviceToken.getText().equals("")) {
		tableDeviceQueryList.removeAll();

		sendXML("authenticateRequest");

		/*
		 * } else { dialog("Already authenticated", SWT.ICON_WARNING | SWT.OK);
		 * 
		 * }
		 */

	}

	private void btnDeviceQueryRequest_Click(SelectionEvent e) {

		if (tableDeviceQueryList.getSelectionCount() > 0) {

			//
			for (TableItem selectedItem : tableDeviceQueryList.getSelection()) {

				String DeviceName = selectedItem.getText(0);

				System.out.println(DeviceName);
				sendXML("deviceQueryRequest");

			}

		} else {

			dialog("Please select a device", SWT.ICON_INFORMATION | SWT.OK);

		}
	}

	private void btnDeviceAcquireRequest_Click(SelectionEvent e) {

		if (tableDeviceAcquireList.getSelectionCount() > 0) {

			//
			for (TableItem selectedItem : tableDeviceAcquireList.getSelection()) {

				String DeviceName = selectedItem.getText(0);
				DeviceIP = selectedItem.getText(1);
				DevicePort = Integer.parseInt(selectedItem.getText(2));

				DeviceToken = textDeviceToken.getText();
				connectDevice(DeviceIP, DevicePort, DeviceName);

			}

		} else {

			dialog("Please select a device", SWT.ICON_INFORMATION | SWT.OK);

		}
	}

	private void btnByeRequest_Click(SelectionEvent e) {

		sendXML("byeRequest");

	}

	private void btnDisconnect_Click(SelectionEvent e) {
		if (!textDeviceToken.getText().equals("")) {

			disconnect();
			clear();

			tabFolder.setSelection(0);

		} else {
			dialog("InterfaceLevel value not found", SWT.ICON_WARNING | SWT.OK);

		}
	}

	private void SendMsg(String XML, CupptSocket socket) {
		try {
			if (XML.contains("{MESSAGEID}"))
				socket.incrementMessageID();

			// replace XML vars
			XML = replace(XML, "{MESSAGEID}", String.valueOf(socket.getMessageID()));
			XML = replace(XML, "{XSDVersion}", XSDVersion);
			XML = replace(XML, "{INTERFACELEVEL}", InterfaceLevel);
			XML = replace(XML, "{AIRLINE}", AirlineCode);
			XML = replace(XML, "{PLATFORMDEFINEDPARAMETER}", "");

			XML = replace(XML, "{APPLICATIONCODE}", ApplicationCode);
			XML = replace(XML, "{APPLICATIONNAME}", ApplicationName);
			XML = replace(XML, "{APPLICATIONVERSION}", ApplicationVersion);
			XML = replace(XML, "{APPLICATIONDATA}", ApplicationData);

			XML = replace(XML, "{DEVICENAME}", socket.getName());
			XML = replace(XML, "{DEVICETOKEN}", DeviceToken);

			XML = replace(XML, "{HOSTNAME}", textHostName.getText());
			XML = replace(XML, "{HOSTIP}", textHostIP.getText());
			XML = replace(XML, "{HOSTPORT}", textHostPort.getText());

			// send header msg
			XML = generateMsgHeader(XML) + XML;

			tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort() + " --> " + XML);

			styledTextEventAppend('S', XML);

			socket.send(XML); // send message

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String generateMsgHeader(String XML) throws UnsupportedEncodingException {
		// send header msg
		String version = "01";
		String validCode = "00";
		String size = "" + Integer.toHexString(XML.getBytes("utf-8").length);
		size = size.toUpperCase();

		while (size.length() < 6)
			size = "0" + size;

		return version + validCode + size.toUpperCase();
	}

	private void clear() {

		styledTextXML.setText("");
		styledTextEvent.setText("");
		styledTextXMLValidate.setText("");
		styledTextByeResponse.setText("");
		textDeviceToken.setText("");

		tableInterfaceLevels.removeAll();
		tableDeviceQueryList.removeAll();

		btnInterfacelevelsavailablerequest.setEnabled(false);
		btnInterfacelevelrequest.setEnabled(false);
		btnUnsupportedInterfacelevelrequest.setEnabled(false);
		btnObsoleteInterfacelevelrequest.setEnabled(false);
		btnAuthenticateRequest.setEnabled(false);

		btnDeviceQueryRequest.setEnabled(false);
		btnDeviceAcquireRequest.setEnabled(false);

		tableDeviceAcquireList.removeAll();
		tableConnectedDeviceList.removeAll();

	}

	/*
	 * Close the Input/Output streams and disconnect not much to do in the catch
	 * clause
	 */
	private void disconnect() {
		try {

			/*
			 * if (socket != null) { System.out.println("Stop Socket");
			 * socket.close(); }
			 * 
			 * if (in != null) { System.out.println("Stop Reading"); in.close();
			 * }
			 * 
			 * if (out != null) { System.out.println("Stop Writing");
			 * out.close(); }
			 */

			System.out.println("Stop " + platformSocket);
			platformSocket.disconnect();

			// devices
			for (CupptSocket socket : deviceSocketList) {

				System.out.println("Stop " + socket);
				socket.disconnect();
			}

		} catch (Exception e) {
			System.out.println("Exception " + e.getMessage());
		}

	}

	private int dialog(String Message, int OPTIONS) {

		// ICON_ERROR, ICON_INFORMATION, ICON_QUESTION, ICON_WARNING,
		// ICON_WORKING
		MessageBox dialog = new MessageBox(shlCupptSdk, OPTIONS);

		dialog.setText("Airline Application");
		dialog.setMessage(Message);
		return dialog.open();

	}

	private void tableEventAddLine(String text) {

		TableItem item = new TableItem(tableEvents, SWT.NONE);
		item.setText(0, getDate());
		item.setText(1, text);

		System.out.println(text);
	}

	private void styledTextEventAppend(char T, String str) {

		String symbol = (T == 'S') ? "-->" : "<--";

		styledTextEvent.append(T + " " + symbol + " " + getDate() + " :\n");
		styledTextEvent.append(str);
		styledTextEvent.append("\n\r");
	}

	// donne la date avec un format par défaut
	private String getDate() {
		return getDate("dd/MM/yyyy HH:mm:ss.SSS");
	}

	// surcharge de la fonction précédente avec le choix du format
	private String getDate(String format) {
		DateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(new Date());
	}

	private void generateXML(String type) {
		// header
		String XML = "<cupps xmlns=\"http://www.cupps.aero/cupps/01.03\" messageID=\"{MESSAGEID}\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" messageName = \""
				+ type + "\">";

		if (type.equals("interfaceLevelsAvailableRequest")) {
			XML += "<interfaceLevelsAvailableRequest hsXSDVersion=\"{XSDVersion}\"/>";
		} else if (type.equals("interfaceLevelRequest")) {
			XML += "<interfaceLevelRequest level=\"{INTERFACELEVEL}\"/>";
		} else if (type.equals("authenticateRequest")) {
			XML += "<authenticateRequest airline=\"{AIRLINE}\" eventToken=\"{APPLICATIONCODE}\" platformDefinedParameter=\"{PLATFORMDEFINEDPARAMETER}\" >";

			XML += "<applicationList>";
			XML += "<application applicationName=\"{APPLICATIONNAME}\" applicationVersion=\"{APPLICATIONVERSION}\" applicationData=\"{APPLICATIONDATA}\"/>";
			XML += "</applicationList>";

			XML += "</authenticateRequest>";
		} else if (type.equals("deviceQueryRequest")) {
			XML += "<deviceQueryRequest deviceName=\"{DEVICENAME}\"/>";
		} else if (type.equals("deviceAcquireRequest")) {
			XML += "<deviceAcquireRequest deviceName=\"{DEVICENAME}\" deviceToken=\"{DEVICETOKEN}\" airlineID=\"{AIRLINE}\"/>";

			if (!textHostName.getText().equals("") && !textHostIP.getText().equals("")
					&& !textHostPort.getText().equals(""))
				XML += "<relayTo hostName=\"{HOSTNAME}\" ip=\"{HOSTIP}\" port=\"{HOSTPORT}\"/>";
		}

		// end
		XML += "</cupps>";

		styledTextXML.setText(XML);

	}

	private String replace(String originalText, String subStringToFind,
			String subStringToReplaceWith) {
		int s = 0;
		int e = 0;

		StringBuffer newText = new StringBuffer();

		while ((e = originalText.indexOf(subStringToFind, s)) >= 0) {

			newText.append(originalText.substring(s, e));
			newText.append(subStringToReplaceWith);
			s = e + subStringToFind.length();

		}

		newText.append(originalText.substring(s));
		return newText.toString();

	}

	private void XMLReader(String rmessage, CupptSocket socket) {

		int startIndex = rmessage.indexOf('<');

		if (startIndex > -1) {
			try {

				// String prefix = rmessage.substring(0, startIndex);
				// System.out.println("prefix : '" + prefix + "'");
				String XML = rmessage.substring(rmessage.indexOf('<'));

				styledTextXMLValidate.setText("");
				// parse an XML document into a DOM tree
				DocumentBuilder parser;
				parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();

				// Document document = parser.parse(new File("instance.xml"));
				Document document = parser.parse(new InputSource(new StringReader(XML)));

				// show parsed message
				for (int i = 0; i < document.getElementsByTagName("*").getLength(); i++) {

					Node node = document.getElementsByTagName("*").item(i);

					if (node.getNodeType() == Node.ELEMENT_NODE) {

						styledTextXMLValidate.append(node.getNodeName() + "\n");

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								styledTextXMLValidate.append("\t" + attr.getNodeName() + ": "
										+ attr.getNodeValue() + "\n");
							}
						}
					}
				}

				// show parsed message
				for (int i = 0; i < document.getElementsByTagName("*").getLength(); i++) {

					Node node = document.getElementsByTagName("*").item(i);

					if (node.getNodeType() == Node.ELEMENT_NODE) {

						styledTextXMLValidate.append(node.getNodeName() + "\n");

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								styledTextXMLValidate.append("\t" + attr.getNodeName() + ": "
										+ attr.getNodeValue() + "\n");
							}
						}
					}
				}

				Node node = document.getElementsByTagName("cupps").item(0);
				String message = node.getAttributes().getNamedItem("messageName").getNodeValue();

				if (message.equals("interfaceLevelsAvailableResponse")) {

					// activate controls
					btnInterfacelevelrequest.setEnabled(true);
					btnObsoleteInterfacelevelrequest.setEnabled(true);
					btnUnsupportedInterfacelevelrequest.setEnabled(true);

					tabFolder.setSelection(1);

					NodeList nodeList = document.getElementsByTagName("interfaceLevel");

					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							TableItem item = new TableItem(tableInterfaceLevels, SWT.NONE);

							item.setText(0, attrs.getNamedItem("level").getNodeValue());
							item.setText(1, attrs.getNamedItem("wsLocalPath").getNodeValue());
							item.setText(2, attrs.getNamedItem("xsdVersion").getNodeValue());

						}

					}
				} else if (message.equals("interfaceLevelResponse")) {

				} else if (message.equals("authenticateResponse")) {

					// activate controls
					btnDeviceAcquireRequest.setEnabled(true);
					btnDeviceQueryRequest.setEnabled(true);

					node = document.getElementsByTagName("authenticateResponse").item(0);

					if (node.hasAttributes()) {
						textDeviceToken.setText(node.getAttributes().getNamedItem("deviceToken")
								.getNodeValue());
					}

					NodeList nodeList = document.getElementsByTagName("device");

					if (InterfaceLevel.contains("00.")) {
						System.out.println("CUPPT Interface"); // CUPPT
																// interface

						for (int i = 0; i < nodeList.getLength(); i++) {
							node = nodeList.item(i);
							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								TableItem item = new TableItem(tableDeviceQueryList, SWT.NONE);
								TableItem acqItem = new TableItem(tableDeviceAcquireList, SWT.NONE);

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());
								item.setText(1, attrs.getNamedItem("hostName").getNodeValue());
								item.setText(2, attrs.getNamedItem("IP").getNodeValue());
								item.setText(3, attrs.getNamedItem("Port").getNodeValue());
								item.setText(4, attrs.getNamedItem("Vendor").getNodeValue());
								item.setText(5, attrs.getNamedItem("Model").getNodeValue());
								item.setText(6, attrs.getNamedItem("Status").getNodeValue());

								String status = attrs.getNamedItem("Status").getNodeValue()
										.toLowerCase();

								if (status.equalsIgnoreCase("ready")) {
									item.setForeground(6, green);
								} else if (status.equalsIgnoreCase("init")) {
									item.setForeground(6, orange);
								} else {
									item.setForeground(6, red);
								}

								acqItem.setText(0, attrs.getNamedItem("deviceName").getNodeValue());
								acqItem.setText(1, attrs.getNamedItem("IP").getNodeValue());
								acqItem.setText(2, attrs.getNamedItem("Port").getNodeValue());

							}
						}

					} else {
						System.out.println("CUPPS Interface"); // CUPPS
																// Interface

						for (int i = 0; i < nodeList.getLength(); i++) {

							TableItem item = new TableItem(tableDeviceQueryList, SWT.NONE);
							TableItem acqItem = new TableItem(tableDeviceAcquireList, SWT.NONE);
							// TableItem item = new
							// TableItem(tableDeviceAcquireList, SWT.NONE);

							node = nodeList.item(i);

							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());

								acqItem.setText(0, attrs.getNamedItem("deviceName").getNodeValue());

							}

							NodeList children = node.getChildNodes();

							for (int j = 0; j < children.getLength(); j++) {
								Node child = children.item(j);

								if (child.getNodeName().contains("DeviceParameter")) {
									NodeList subchildren = child.getChildNodes();

									for (int k = 0; k < subchildren.getLength(); k++) {
										Node subChild = subchildren.item(k);

										if (subChild.getNodeName() == "ipAndPort") {
											NamedNodeMap attrs = subChild.getAttributes();

											item.setText(1, attrs.getNamedItem("hostName")
													.getNodeValue());

											item.setText(2, attrs.getNamedItem("ip").getNodeValue());
											acqItem.setText(1, attrs.getNamedItem("ip")
													.getNodeValue());

											item.setText(3, attrs.getNamedItem("port")
													.getNodeValue());
											acqItem.setText(2, attrs.getNamedItem("port")
													.getNodeValue());

										}

										if (subChild.getNodeName() == "vendorModelInfo") {
											NamedNodeMap attrs = subChild.getAttributes();

											item.setText(4, attrs.getNamedItem("vendor")
													.getNodeValue());

											item.setText(5, attrs.getNamedItem("model")
													.getNodeValue());

										}

										if (subChild.getNodeName().contains("Status")) {
											NamedNodeMap attrs = subChild.getAttributes();

											for (int l = 0; l < attrs.getLength(); l++) {

												if (attrs.item(l).getNodeValue().equals("true")) {
													item.setText(6, attrs.item(l).getNodeName());
													acqItem.setText(6, attrs.item(l).getNodeName());
												}
											}
										}

									}

								}

							}
						}
					}

					colorizeDeviceStatus();

				} else if (message.equals("deviceQueryResponse")) {

					String response = "deviceQueryResponse \n";

					NodeList nodeList = document.getElementsByTagName("device");
					for (int i = 0; i < nodeList.getLength(); i++) {
						node = nodeList.item(i);

						if (node.hasAttributes()) {
							NamedNodeMap attrs = node.getAttributes();

							for (int j = 0; j < attrs.getLength(); j++) {
								Node attr = attrs.item(j);
								response += attr.getNodeName();
								if (attr.getNodeName().length() <= 11) {
									response += "\t";
								}
								if (attr.getNodeName().length() <= 6) {
									response += "\t";
								}
								response += ": " + attr.getNodeValue() + "\n";
							}
						}
					}

					dialog(response, SWT.ICON_WORKING);

				} else if (message.equals("deviceAcquireResponse")) {

					NodeList nodeList = document.getElementsByTagName("device");

					if (InterfaceLevel.contains("00.")) {
						System.out.println("CUPPT Interface"); // CUPPT
																// interface

						for (int i = 0; i < nodeList.getLength(); i++) {
							node = nodeList.item(i);
							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								TableItem item = new TableItem(tableConnectedDeviceList, SWT.NONE);

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());
								item.setText(1, attrs.getNamedItem("IP").getNodeValue());
								item.setText(2, attrs.getNamedItem("Port").getNodeValue());
								item.setText(3, attrs.getNamedItem("Status").getNodeValue());

								String status = attrs.getNamedItem("Status").getNodeValue()
										.toLowerCase();

								if (status.equalsIgnoreCase("ready")) {
									item.setForeground(3, green);
								} else if (status.equalsIgnoreCase("paperOut")
										|| status.equalsIgnoreCase("paperJam")
										|| status.equalsIgnoreCase("diskError")
										|| status.equalsIgnoreCase("init")) {
									item.setForeground(3, orange);
								} else {
									item.setForeground(3, red);
								}

							}
						}

					} else {
						System.out.println("CUPPS Interface"); // CUPPS
																// Interface

						for (int i = 0; i < nodeList.getLength(); i++) {

							TableItem item = new TableItem(tableConnectedDeviceList, SWT.NONE);

							node = nodeList.item(i);

							if (node.hasAttributes()) {
								NamedNodeMap attrs = node.getAttributes();

								item.setText(0, attrs.getNamedItem("deviceName").getNodeValue());
							}

							NodeList children = node.getChildNodes();

							for (int j = 0; j < children.getLength(); j++) {
								Node child = children.item(j);

								if (child.getNodeName() == "ipAndPort") {
									NamedNodeMap attrs = child.getAttributes();

									item.setText(1, attrs.getNamedItem("ip").getNodeValue());
									item.setText(2, attrs.getNamedItem("Port").getNodeValue());

								}

								if (child.getNodeName() == "Status") {
									NamedNodeMap attrs = child.getAttributes();

									for (int k = 0; k < attrs.getLength(); k++) {

										if (attrs.item(k).getNodeValue().equals("true")) {
											item.setText(3, attrs.item(k).getNodeName());
										}
									}
								}
							}
						}
					}

				} else if (message.equals("notify")) {

					NodeList children = document.getElementsByTagName("*");

					for (int j = 0; j < children.getLength(); j++) {
						Node child = children.item(j);

						if (child.getNodeName().equals("DeviceStatusNotification")) {

							if (child.hasAttributes()) {
								NamedNodeMap attrs = child.getAttributes();
								String status = attrs.getNamedItem("ddStatus").getNodeValue();
								/*
								 * dialog(socket.getName() + " Status : "
								 * + attrs.getNamedItem("ddStatus"
								 * ).getNodeValue( ), SWT.ICON_WARNING);
								 */

								for (int i = 0; i < tableConnectedDeviceList.getItemCount(); i++) {
									TableItem item = tableConnectedDeviceList.getItem(i);

									item.setText(3, status);

									if (socket.getIP().equals(item.getText(1))
											&& socket.getPort() == Integer
													.parseInt(item.getText(2))) {
										if (item.getText(3).equalsIgnoreCase("ready")) {
											item.setForeground(3, green);
										} else if (item.getText(3).equals("paperOut")
												|| item.getText(3).equals("paperJam")
												|| item.getText(3).equals("diskError")
												|| item.getText(3).equals("init")) {
											item.setForeground(3, orange);
										} else {
											item.setForeground(3, red);
										}
									}

								}
							}
						}
					}

				} else if (message.equals("byeResponse")) {

					node = document.getElementsByTagName("byeResponse").item(0);
					if (node.hasAttributes()) {
						String response = node.getAttributes().getNamedItem("result")
								.getNodeValue();

						if (response.equals("OK")) {

						}

					}

				}
			}

			catch (ParserConfigurationException e1) {

				styledTextXMLValidate.setText("ParserConfigurationException : " + e1.getMessage());

			} catch (SAXParseException spe) {

				StringBuffer sb = new StringBuffer(spe.getMessage());
				sb.append("\n\tLine number: " + spe.getLineNumber());
				sb.append("\n\tColumn number: " + spe.getColumnNumber());

				styledTextXMLValidate.setText("SAXParseException : " + sb.toString());

			} catch (SAXException e1) {

				styledTextXMLValidate.setText("SAXException : " + e1.getMessage());

			} catch (IOException e1) {

				styledTextXMLValidate.setText("IOException : " + e1.getMessage());

			} catch (Exception e1) {

				styledTextXMLValidate.setText("Exception when extracting XML data");

			}
		}

	}

	private void colorizeDeviceStatus() {

		for (int i = 0; i < tableDeviceQueryList.getItemCount(); i++) {
			TableItem item = tableDeviceQueryList.getItem(i);

			if (item.getText(6).equalsIgnoreCase("ready")) {
				item.setForeground(6, green);
			} else if (item.getText(6).equalsIgnoreCase("init")) {
				item.setForeground(6, orange);
			} else {
				item.setForeground(6, red);
			}

		}
	}

	private void sendXML(String XML) {
		sendXML(XML, platformSocket);
	}

	// override function
	private void sendXML(String XML, CupptSocket socket) {

		generateXML(XML);
		// send message
		SendMsg(styledTextXML.getText(), socket);
	}

	private void connectDevice(String dIP, int dPort, String dName) {
		try {

			System.out.println("try to connect");

			CupptSocket deviceSocket = new CupptSocket(dIP, dPort, dName);

			// Init ObservableListener
			final ObservableListener ObservableDeviceListener = new ObservableListener(deviceSocket);

			deviceSocket.setObservableListener(ObservableDeviceListener);

			// Add observator
			deviceSocket.getObservableListener().addObservateur(new Observateur() {
				public void update(String message) {

					final String XML = message;

					Display.getDefault().asyncExec(new Runnable() {
						public void run() {

							CupptSocket socket = ObservableDeviceListener.getSocket();

							styledTextEventAppend('R', XML);

							tableEventAddLine("Message " + socket.getIP() + ":" + socket.getPort()
									+ " <-- " + XML);

							XMLReader(XML, socket);
						}
					});

				}
			});

			deviceSocketList.add(deviceSocket);

			System.out.println(deviceSocket.getName() + " connected !");

			sendXML("deviceAcquireRequest", deviceSocket);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			tableEventAddLine("Exception : " + e.getMessage());
			// dialog("Unable to connect to " + dIP + ":" + dPort + "\n" +
			// e.getMessage(),SWT.ICON_ERROR);
		}
	}

	/*  */

}
